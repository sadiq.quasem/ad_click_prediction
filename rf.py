import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
import pickle
from src.datasets import *

rf_model_final = RandomForestClassifier(class_weight='balanced')
rf_model_final.fit(df_train_features, df_train_class.values.ravel())

predicted_train = rf_model_final.predict(df_train_features)
predicted_test = rf_model_final.predict(df_test_features)

predicted_prob_train = rf_model_final.predict_proba(df_train_features)
predicted_prob_test = rf_model_final.predict_proba(df_test_features)

with open('./models/rf_model.sav', 'wb') as f:
    pickle.dump(rf_model_final, f)
