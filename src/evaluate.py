from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.metrics import precision_score, recall_score, f1_score
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import classification_report

import matplotlib.pyplot as plt

'''
Method for evaluating classifier performance
'''

def evaluate(df_train_class, predicted_train, predicted_prob_train, df_test_class, predicted_test, predicted_prob_test):
    '''
    Confusion Matrix
    '''
    confusion_matrix_train = confusion_matrix(df_train_class, predicted_train)
    confusion_matrix_test = confusion_matrix(df_test_class, predicted_test)
    print ("\nTraining Confusion Matrix:\n ", confusion_matrix_train)
    print ("\nTesting Confusion Matrix:\n ", confusion_matrix_test)

    '''
    Accuracy Score
    '''
    score_train = accuracy_score(df_train_class, predicted_train)
    score_test = accuracy_score(df_test_class, predicted_test)
    print ("\nTraining Accuracy Score: ", score_train)
    print ("\nTesting Accuracy Score: ", score_test)

    '''
    Precision and Recall
    '''
    precision_train = precision_score(df_train_class, predicted_train, average='micro')
    precision_test = precision_score(df_test_class, predicted_test, average='micro')
    print ("\nTraining Precision: ", precision_train)
    print ("\nTesting Precision: ", precision_test)

    recall_train = recall_score(df_train_class, predicted_train, average='micro')
    recall_test = recall_score(df_test_class, predicted_test,  average='micro')
    print ("\nTraining Recall: ", recall_train)
    print ("\nTesting Recall: ", recall_test)

    '''
    Classification Report
    '''
    print ("\nTrain Classification Report: \n",classification_report(df_train_class, predicted_train))
    print ("\nTest Classification Report: \n",classification_report(df_test_class, predicted_test))


    '''
    F1 Score
    '''
    f1score_train = f1_score(df_train_class, predicted_train, average='micro')
    f1score_test = f1_score(df_test_class, predicted_test, average='micro')
    print ("\nTraining F1score: ", f1score_train)
    print ("\nTesting F1score: ", f1score_test)

    f1score_train = f1_score(df_train_class, predicted_train, average='micro')
    f1score_test = f1_score(df_test_class, predicted_test, average='micro')
    print ("\nTraining Weighted F1score: ", f1score_train)
    print ("\nTesting Weighted F1score: ", f1score_test)
