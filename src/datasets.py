import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

def dataset():
    #read dataset and drop unnamed:0 column
    df = pd.read_csv('./data/modified/true_clicks_processed.csv', index_col = [0])

    def separate_train_test(df):
        df_class = pd.DataFrame(df['y'])
        df_features = df.loc[:, df.columns != 'y']
        # df_features = df.iloc[:, 0:10]

        df_features_train, df_features_test, df_class_train,  df_class_test = train_test_split(df_features, df_class)

        df_train = pd.concat([df_features_train, df_class_train], axis=1)
        df_test = pd.concat([df_features_test, df_class_test], axis=1)

        return df_train, df_test

    df_train, df_test = separate_train_test(df)

    '''
    Set features and target columns
    '''
    df_train_class = pd.DataFrame(df_train['y'])
    df_train_features = df_train.loc[:, df_train.columns != 'y']

    df_test_class = pd.DataFrame(df_test['y'])
    df_test_features = df_test.loc[:, df_test.columns != 'y']

    return df_train_features, df_train_class, df_test_class, df_test_features

df_train_features, df_train_class, df_test_class, df_test_features = dataset()
