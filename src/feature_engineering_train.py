import numpy as np
import pandas as pd

def preprocess():
    df = pd.read_csv('./data/original/true_clicks.csv')
    # df = pd.read_csv('./data/original/to_predict.csv')
    df = df[['ad_num']].join(pd.get_dummies(df['user_num'])).groupby('ad_num').max()
    df = df.replace(0, np.nan)
    s = df.apply(pd.Series.last_valid_index)
    '''
    This removes the last ad in the one hot encoded features
    '''
    df.values[df.index.get_indexer(s),df.columns.get_indexer(s.index.tolist())]=np.nan
    df = df.T
    df = df.replace(np.nan, 0)
    df = pd.concat([df, s], axis=1)
    '''
    df.rename(columns={0: 'y'}, inplace=True)
    df.to_csv('script_testing.csv', header=None, index=None)
    '''
    df = df.reset_index()
    df=df.drop('index', axis=1)
    return (df)

df = preprocess()

def feature(df):
    def func(x):
        # create result array
        result = np.zeros(x.shape, dtype=np.int)

        # get indices of array distinct of zero
        w = np.argwhere(x).ravel()

        # compute the difference between consecutive indices and add the first index + 1
        array = np.hstack(([w[0] + 1], np.ediff1d(w)))

        # set the values on result
        np.put(result, w, array)

        return result


    c = ['S{}'.format(i) for i in range(1, 12)]
    s = pd.DataFrame(df.ne(0).apply(func, axis=1).values.tolist(), columns=c)

    result = pd.concat([df, s], axis=1)
    result = result.drop('S11', axis=1)

    y = result.pop(0)
    result['y']=y

    # u = result.iloc[:, :10]
    # result[u.columns] = u[~u.iloc[:, ::-1].cumsum(1).le(1)].fillna(0, downcast='infer')

    result = result[list(result)].astype(int)
    result.to_csv('./data/modified/true_clicks_processed.csv')
    # result.to_csv('./data/modified/to_predict_processed.csv')

feature(df)
