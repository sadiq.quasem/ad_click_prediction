import numpy as np
import pandas as pd
import pickle
from src.datasets import *
from sklearn.linear_model import LogisticRegression

lr_model_final = LogisticRegression(C=1e5, solver='lbfgs', multi_class='multinomial', max_iter=100)
lr_model_final.fit(df_train_features, df_train_class.values.ravel())

predicted_train = lr_model_final.predict(df_train_features)
predicted_test = lr_model_final.predict(df_test_features)

predicted_prob_train = lr_model_final.predict_proba(df_train_features)
predicted_prob_test = lr_model_final.predict_proba(df_test_features)

with open('./models/lr_model.sav', 'wb') as f:
    pickle.dump(lr_model_final, f)
