import numpy as np
import pandas as pd
from sklearn.svm import SVC
import pickle
from src.datasets import *

svm_model_final = SVC(C = 1.5, gamma = 0.05, kernel = 'rbf', class_weight='balanced', probability=True)
svm_model_final.fit(df_train_features, df_train_class.values.ravel())

predicted_train = svm_model_final.predict(df_train_features)
predicted_test = svm_model_final.predict(df_test_features)

predicted_prob_train = np.array([])
predicted_prob_test = np.array([])

predicted_prob_train = svm_model_final.predict_proba(df_train_features)
predicted_prob_test  = svm_model_final.predict_proba(df_test_features)

with open('./models/svm_model.sav', 'wb') as f:
    pickle.dump(svm_model_final, f)
