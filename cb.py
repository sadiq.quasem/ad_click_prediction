import numpy as np
import pandas as pd
import hyperopt
from catboost import CatBoostClassifier, Pool, cv
from catboost import MetricVisualizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from numpy.random import RandomState
from src.datasets import *

categorical_features_indices = np.where(df_train_features.dtypes != np.float)[0]

model = CatBoostClassifier(
    custom_loss=['TotalF1'],
    random_seed=42,
    logging_level='Silent',
    loss_function='MultiClass',
    use_best_model=True
)

model.fit(
    df_train_features, df_train_class,
    cat_features=categorical_features_indices,
    eval_set=(df_test_features, df_test_class),
    # logging_level='Verbose',
    plot=True
)

train_pool = Pool(df_train_features, df_train_class, cat_features=categorical_features_indices)
validate_pool = Pool(df_test_features, df_test_class, cat_features=categorical_features_indices)

feature_importances = model.get_feature_importance(train_pool)
feature_names = df_train_features.columns
for score, name in sorted(zip(feature_importances, feature_names), reverse=True):
    print('{}: {}'.format(name, score))

df2 = pd.read_csv('./data/modified/to_predict_processed.csv', index_col = [0])
predictions = model.predict(df2)
predictions_probs = model.predict_proba(df2)
print(predictions)
print(predictions_probs)

model.save_model('./models/catboost_model.dump')
